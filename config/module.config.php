<?php
/**
 * glidepayments config
 *
 * @category Config
 * @package  GlidePayments
 * @author   Stewart Megaw
 */

namespace GlidePayments;

return array(
    'service_manager' => array(
        'factories' => array(
            // service factories
            'GlidePayments\Service\Payment' => 'GlidePayments\Service\PaymentServiceFactory',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            // controllers factories
            'GlidePayments\Controller\Paypal' => 'GlidePayments\Controller\PaypalControllerFactory',
        )
    ),
    //router config
    'router' => array(
        'routes' => array(
            'payment' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/payment',
                    'defaults' => array(
                        'controller' => 'GlidePayments\Controller\Paypal',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(),
            ),
            'payment_pay' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/payment/:amount[/:item_id]',
                    'defaults' => array(
                        'controller' => 'GlidePayments\Controller\Paypal',
                        'action' => 'index',
                    ),
                ),
            ),
            'payment_capture' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/payment/capture/:user',
                    'defaults' => array(
                        'controller' => 'GlidePayments\Controller\Paypal',
                        'action' => 'capture',
                    ),
                ),
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
    ),
);
