<?php
/**
 * Module.php
 * 
 * This file is placed here for compatibility with ZendFramework 2's ModuleManager.
 * It allows usage of this module even without composer.
 * 
 * @category ZF2 Module File
 * @package  GlidePayments
 * @author   Stewart Megaw
 */

require_once __DIR__ . '/src/GlidePayments/Module.php';