<?php
/**
 * PaymentService.php
 *
 * @category ServiceFactory
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Service;

use Doctrine\ORM\EntityManager;
use GlidePayments\Entity\Payment;
use GlidePayments\Entity\PaymentEscrowed;
use GlidePayments\Entity\PaymentItemInterface;
use GlidePayments\Entity\UserInterface;
use GlidePayments\DPayPal\DPayPal;

/**
 * PaypalControllerService provides functions for handling payments
 */
class PaymentService
{

    private $entityManager;
    private $authentication;
    private $config;

    public function __construct(EntityManager $entityManager, $config, $authentication) 
    {
        $this->entityManager = $entityManager;
        $this->authentication = $authentication;
        $this->config = $config;
    }

    /**
     * Returns the users current account balance minus any
     * escrowed funds
     *
     * @param  User $user Optional pass a user. If empty then the currently authenticated user will used
     * @return double $balance Account balance
     */
    public function getUserAccountBalance($user = null) 
    {
        if (empty($user)) {
            $user = $this->authentication->getIdentity();
        }

        $balance = 0;

        // Get all payments to/from a user
        $query = $this->entityManager->createQuery(
            "
                SELECT p
                FROM GlidePayments\Entity\Payment p
                WHERE (p.userFrom = :user OR p.userTo = :user)
                AND p.status = :status
            "
        );
        $query->setParameter('user', $user);
        $query->setParameter('status', Payment::PAYMENT_STATUS_CONFIRMED);
        $payments = $query->getResult();

        foreach ($payments as $payment) {
            if ($payment->getUserTo() == $user) {
                $balance += $payment->getAmount();
            } else if ($payment->getUserFrom() == $user) {
                $balance -= $payment->getAmount();
            }
        }

        // Get all payments currently escrowed against user
        $query = $this->entityManager->createQuery(
            "
                SELECT e
                FROM GlidePayments\Entity\PaymentEscrowed e
                WHERE e.user = :user
                AND e.status = :status
            "
        );
        $query->setParameter('user', $user);
        $query->setParameter('status', PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ACTIVE);
        $payments = $query->getResult();

        foreach ($payments as $payment) {
            $balance -= (float) $payment->getAmount();
        }

        return $balance;
    }

    /**
     * Returns any escrowed transactions
     *
     * @param  array $status_types Status types to filter by
     * @param  User  $user         Optional pass a user. If empty then the currently authenticated user will used
     * @return array $escrowedPayments
     */
    public function getEscrowedTransactions($status_types = array(), $user = null) 
    {
        if (empty($user)) {
            $user = $this->authentication->getIdentity();
        }

        // Get all payments currently escrowed against user
        $query = $this->entityManager->createQuery(
            "
                SELECT  e
                FROM GlidePayments\Entity\PaymentEscrowed e
                WHERE e.user = :user
                AND e.status IN (:status)
            "
        );
        $query->setParameter('user', $user);
        $query->setParameter('status', $status_types);
        $escrowedPayments = $query->getResult();

        return $escrowedPayments;
    }

    /**
     * Returns all transactions into and out of a users account
     *
     * @param  int  $limit Limit the number of transactions to return
     * @param  User $user  Optional pass a user. If empty then the currently authenticated user will used
     * @return array $transactions
     */
    public function getInOutTransactions($limit = 100, $user = null) 
    {
        if (empty($user)) {
            $user = $this->authentication->getIdentity();
        }

        $balance = 0;

        // Get all payments to/from a user
        $query = $this->entityManager->createQuery(
            "
                SELECT p.id, p.amount, p.generatedTimestamp, p.userToPaypalEmail,
                uf.id AS userFromId, uf.firstname AS userFromFirstname, uf.lastname AS userFromLastname,
                ut.id AS userToId, ut.firstname AS userToFirstname, ut.lastname AS userToLastname
                FROM GlidePayments\Entity\Payment p
                JOIN p.userFrom uf
                JOIN p.userTo ut
                WHERE (uf = :user OR ut = :user)
                AND p.status = :status
                ORDER BY p.generatedTimestamp DESC
            "
        );
        $query->setParameter('user', $user);
        $query->setParameter('status', Payment::PAYMENT_STATUS_CONFIRMED);
        $transactions = $query->getArrayResult();

        return $transactions;
    }

    /**
     * Sets an escrow payment item to canceled
     * 
     * @param  GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     * @return null
     */
    public function cancelEscrow(PaymentItemInterface $paymentItem) 
    {
        if ($this->isActiveEscrowed($paymentItem)) {
            $paymentEscrowRepo = $this->entityManager->getRepository('GlidePayments\Entity\PaymentEscrowed');
            $escrowPayment = $paymentEscrowRepo->findOneBy(array('paymentItem' => $paymentItem));
            $escrowPayment->setStatus(PaymentEscrowed::PAYMENT_ESCROWED_TYPE_CANCELED);
            $this->entityManager->persist($escrowPayment);
        }
    }

    /**
     * Places a payment item into escrow
     * 
     * @param  GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     * @return null
     */
    public function escrowPayment(PaymentItemInterface $paymentItem) 
    {
        $paymentEscrowRepo = $this->entityManager->getRepository('GlidePayments\Entity\PaymentEscrowed');
        if (!empty($paymentEscrowRepo->findOneBy(array('paymentItem' => $paymentItem, 'status' => PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ACTIVE)))) {
            throw new \Exception('Escrow exists for this payment entity');
        }

        if ($this->getRequiredTopup($paymentItem) > 0) {
            throw new \Exception('Not enought funds');
        }

        $producerFeePercentage = $this->config['GlidePayments']['Fees']['Producer'];
        $customerFeePercentage = $this->config['GlidePayments']['Fees']['Customer'];

        $escrowPayment = new PaymentEscrowed();
        $escrowPayment->setUser($this->authentication->getIdentity());
        $escrowPayment->setStatus(PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ACTIVE);
        $escrowPayment->setPaymentItem($paymentItem);
        $escrowPayment->setAmount($paymentItem->getAmount() * (1 + $customerFeePercentage));
        $escrowPayment->setProducerFee($paymentItem->getAmount() * $producerFeePercentage);
        $this->entityManager->persist($escrowPayment);
    }

    /**
     * Check if a payment item is currently in escrow
     * 
     * @param  GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     * @return boolean
     */
    public function isActiveEscrowed(PaymentItemInterface $paymentItem) 
    {
        $paymentEscrowRepo = $this->entityManager->getRepository('GlidePayments\Entity\PaymentEscrowed');
        $escrow = $paymentEscrowRepo->findOneBy(array('paymentItem' => $paymentItem, 'status' => PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ACTIVE));

        if (empty($escrow)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the required topup amount needed to place $paymentItem into escrow
     * 
     * @param  GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     * @return decimal The topup amount
     */
    public function getRequiredTopup(PaymentItemInterface $paymentItem) 
    {
        $balance = $this->getUserAccountBalance();

        $customerFeePercentage = $this->config['GlidePayments']['Fees']['Customer'];
        $paymentItemAmount = $paymentItem->getAmount();

        $topup = ($paymentItemAmount * (1 + $customerFeePercentage)) - $balance;

        return \max(array(0, round($topup, 2)));
    }

    /**
     * Returns the provider fee for a given $paymentItem
     * 
     * @param  GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     * @return decimal Fee
     */
    public function getProveriderFee(PaymentItemInterface $paymentItem) 
    {
        $balance = $this->getUserAccountBalance();

        $paymentItemAmount = $paymentItem->getAmount();

        if ($paymentItem->getProducer() == $this->authentication->getIdentity()) {
            $feePercentage = $this->config['GlidePayments']['Fees']['Producer'];
        } else {
            $feePercentage = $this->config['GlidePayments']['Fees']['Customer'];
        }



        return round($paymentItemAmount * $feePercentage, 2);
    }

    /**
     * Sets an escrow status to completed. Creates the required user transactions and distributes
     * funds to the relevant parties
     * 
     * @param GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     * @param GlidePayments\Entity\UserInterface        $customer    The customer
     */
    public function completeEscrow(PaymentItemInterface $paymentItem, UserInterface $customer) 
    {
        $paymentEscrowRepo = $this->entityManager->getRepository('GlidePayments\Entity\PaymentEscrowed');
        $escrow = $paymentEscrowRepo->findOneBy(array('paymentItem' => $paymentItem, 'status' => PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ACTIVE));

        if (empty($escrow)) {
            throw new \Exception('No active escrow exists for this payment entity');
        }

        $userRepo = $this->entityManager->getRepository($this->config['GlidePayments']['UserEntityName']);

        // 1) Fee to service provider from customer
        $fee = round($escrow->getAmount() - $paymentItem->getAmount(), 2);
        if ($fee > 0) {
            $payment1 = new Payment();
            $payment1->setAmount($fee);
            $payment1->setStatus(Payment::PAYMENT_STATUS_CONFIRMED);
            $payment1->setVia(Payment::PAYMENT_VIA_INTERNAL);
            $payment1->setUserTo($userRepo->find($this->config['GlidePayments']['Provider_user_id']));
            $payment1->setUserFrom($customer);
            $payment1->setEscrow($escrow);
            $this->entityManager->persist($payment1);
        }

        // 2) Fee to service provider from customer
        if ($escrow->getProducerFee() > 0) {
            $payment2 = new Payment();
            $payment2->setAmount($escrow->getProducerFee());
            $payment2->setStatus(Payment::PAYMENT_STATUS_CONFIRMED);
            $payment2->setVia(Payment::PAYMENT_VIA_INTERNAL);
            $payment2->setUserTo($userRepo->find($this->config['GlidePayments']['Provider_user_id']));
            $payment2->setUserFrom($paymentItem->getProducer());
            $payment2->setEscrow($escrow);
            $this->entityManager->persist($payment2);
        }

        // 3) Fee to producer from customer
        $payment3 = new Payment();
        $payment3->setAmount($paymentItem->getAmount());
        $payment3->setStatus(Payment::PAYMENT_STATUS_CONFIRMED);
        $payment3->setVia(Payment::PAYMENT_VIA_INTERNAL);
        $payment3->setUserTo($paymentItem->getProducer());
        $payment3->setUserFrom($customer);
        $payment3->setEscrow($escrow);
        $this->entityManager->persist($payment3);


        $escrow->setStatus(PaymentEscrowed::PAYMENT_ESCROWED_TYPE_COMPLETE);
        $this->entityManager->persist($escrow);

        return true;
    }

    /**
     * Flags an escrow as being in arbitration. Escrow cannot be completed until the arbitration
     * is resolved
     * 
     * @param GlidePayments\Entity\PaymentItemInterface $paymentItem The payment item
     */
    public function startArbitration(PaymentItemInterface $paymentItem) 
    {
        $paymentEscrowRepo = $this->entityManager->getRepository('GlidePayments\Entity\PaymentEscrowed');
        $escrow = $paymentEscrowRepo->findOneBy(array('paymentItem' => $paymentItem, 'status' => PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ACTIVE));

        if (empty($escrow)) {
            throw new \Exception('No active escrow exists for this payment entity');
        }

        if ($this->authentication->getIdentity() == $escrow->getUser()) {
            $type = PaymentEscrowed::PAYMENT_ESCROWED_ARB_CUSTOMER;
        } else {
            $type = PaymentEscrowed::PAYMENT_ESCROWED_ARB_PRODUCER;
        }

        $escrow->setStatus(PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ARB);
        $escrow->setArbitrationStatus($type);
        $this->entityManager->persist($escrow);

        return true;
    }

    /**
     * Completes an abitrated escrow.  Creates the required user transactions and distributes
     * funds to the relevant parties. After customer & producer have received their funds (if any)
     * the remaining funds are transferred to the service provider
     * 
     * @param double                                    $customerPayment The dollar payment value for the customer
     * @param double                                    $producerPayment The dollar payment value for the producer
     * @param GlidePayments\Entity\PaymentItemInterface $paymentItem     The payment item
     */
    public function completeArbitration(PaymentItemInterface $paymentItem, $customerPayment, $producerPayment) 
    {
        $paymentEscrowRepo = $this->entityManager->getRepository('GlidePayments\Entity\PaymentEscrowed');
        $escrow = $paymentEscrowRepo->findOneBy(array('paymentItem' => $paymentItem, 'status' => PaymentEscrowed::PAYMENT_ESCROWED_TYPE_ARB));
        if (empty($escrow)) {
            throw new \Exception('No arb escrow exists for this payment entity');
        }

        // TODO - Divide payment amongst parties. Remainder goes to service provider

        $escrow->setStatus(PaymentEscrowed::PAYMENT_ESCROWED_TYPE_COMPLETE);
        $this->entityManager->persist($escrow);

        return true;
    }

    /**
     * Sends funds from the authenticated users account to an email address of their choice using
     * Paypal Mass Pay. If Mass Pay is disabled then the payment out is still created and you must
     * manually handle this
     * 
     * @param double $amount         The dollar payment value
     * @param string $emailRecipient The Paypal email address where the money should be sent
     */
    public function withdrawPayment($amount, $emailRecipient) 
    {
        $balance = $this->getUserAccountBalance();

        if ($balance - $amount < 0) {
            throw new \Exception('Amount greater than account balance');
        }

        if (!filter_var($emailRecipient, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Invalid email');
        }

        if ($this->config['GlidePayments']['PaypalMassPayEnabled'] === true) {
            // Create Paypal request as per 
            // http://stackoverflow.com/questions/25893529/paypal-mass-payment-nvp-api
            $paypal = new DPayPal($this->config['GlidePayments']['Paypal']);

            $requestParams = array(
                'EMAILSUBJECT' => $this->config['GlidePayments']['PaypalPayoutEmailSubject'],
                'RECEIVERTYPE' => 'EmailAddress',
                'CURRENCYCODE' => 'USD',
                'L_EMAIL0' => $emailRecipient,
                'L_Amt0' => $amount,
                'L_UNIQUEID0' => time(), // TODO - update this
                'L_NOTE0' => $this->config['GlidePayments']['PaypalPayoutNote'],
            );

            $response = $paypal->MassPayout($requestParams);

            if (is_array($response) && $response['ACK'] == 'Success') { //Request successful
                $this->withdrawalSuccessful($amount, $emailRecipient);
                return true;
            } else {
                throw new \Exception('Payout failed');
            }
        }
        else
        {
            $this->withdrawalSuccessful($amount, $emailRecipient);
            return true;
        }
    }

    private function withdrawalSuccessful($amount, $emailRecipient) 
    {
        $userRepo = $this->entityManager->getRepository($this->config['GlidePayments']['UserEntityName']);

        $payment = new Payment();
        $payment->setAmount($amount);
        $payment->setStatus(Payment::PAYMENT_STATUS_CONFIRMED);
        $payment->setVia(Payment::PAYMENT_VIA_PAYPAL);
        $payment->setUserTo($userRepo->find($this->config['GlidePayments']['Paypal_user_id']));
        $payment->setUserFrom($this->authentication->getIdentity());
        $payment->setUserToPaypalEmail($emailRecipient);
        $this->entityManager->persist($payment);
    }

}
