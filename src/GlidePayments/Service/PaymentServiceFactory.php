<?php
/**
 * PaymentServiceFactory.php
 *
 * @category ServiceFactory
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * PaymentServiceFactory creates a PaymentService object
 * using dependency injection
 */
class PaymentServiceFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator) 
    {
        //dependencies
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $config = $serviceLocator->get('Config');
        $authUserService = $serviceLocator->get($config['GlidePayments']['UserAuthenticationService']);

        //dependency injections
        return new PaymentService($entityManager, $config, $authUserService);
    }

}
