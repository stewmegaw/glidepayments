<?php
/**
 * PaypalController.php
 *
 * @category Controller
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Controller;

use GlidePayments\DPayPal\DPayPal;
use GlidePayments\Entity\Payment;
use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\ORM\EntityManager;

/**
 * PaypalController handles actions called by the router
 */
class PaypalController extends AbstractActionController
{

    private $objectManager;
    private $config;
    private $authentication;

    public function __construct(EntityManager $objectManager, $config, $authentication) 
    {
        $this->objectManager = $objectManager;
        $this->config = $config;
        $this->authentication = $authentication;
    }

    /**
     * This route is for users transferring money into the MVP account.
     * Amount is required as a route parameter. Return route is an optional query string parameter.
     * If specified it should be a complete URL.
     *
     * @return null
     */
    public function indexAction() 
    {
        $return_url = $this->params()->fromQuery('return', null);
        
        $cancel_url = $this->params()->fromQuery('cancel', null);

        if (empty($return_url)) {
            $return_url = $this->url()->fromRoute($this->config['GlidePayments']['DefaultReturnRoute'][0], $this->config['GlidePayments']['DefaultReturnRoute'][1], $this->config['GlidePayments']['DefaultReturnRoute'][2]);
        }
        
        if (empty($cancel_url)) {
            $cancel_url = $return_url;
        }

        $user = $this->authentication->getIdentity();
        // Incase the application has not already redirected non authenticated users
        if (empty($user)) {
            return $this->redirect()->toUrl($return_url);
        }

        $user_id = $user->getId();
        $amount = $this->params()->fromRoute('amount');
        $item_id = $this->params()->fromRoute('item_id', null);

        $paypal = new DPayPal($this->config['GlidePayments']['Paypal']); //Create an DPayPal object

        $requestParams = array(
            //Enter URL of the page where you want to redirect your user after user enters PayPal login data and confirms order on PayPal page
            'RETURNURL' => $this->url()->fromRoute(
                'payment_capture', array('user' => $user_id), array('force_canonical' => true, 'query' => array(
                    'return' => rawurlencode($return_url)
                ))
            ),
            //Page you want to redirect user to, if user press cancel button on PayPal website
            'CANCELURL' => $cancel_url,
        );

        $orderParams = array(
            'LOGOIMG' => "", //URL of your website logo. This image which will be displayed to the customer on the PayPal checkout page
            "MAXAMT" => "10000", //Set maximum amount of transaction
            "NOSHIPPING" => "1", //I do not want shipping
            "ALLOWNOTE" => "0", //I do not want to allow notes
            "BRANDNAME" => $this->config['GlidePayments']['PayPal']['CompanyName'],
            "GIFTRECEIPTENABLE" => "0", //Disable gift receipt widget on the PayPal pages
            "GIFTMESSAGEENABLE" => "0"//Disables the gift message widget on the PayPal pages
        );

        // Item settings
        $item = array(
            'PAYMENTREQUEST_0_AMT' => $amount,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
            'PAYMENTREQUEST_0_ITEMAMT' => $amount,
            'L_PAYMENTREQUEST_0_NAME0' => 'Transfer',
            'L_PAYMENTREQUEST_0_DESC0' => 'Top up funds',
            'L_PAYMENTREQUEST_0_AMT0' => $amount,
            'L_PAYMENTREQUEST_0_QTY0' => '1',
        );

        if(!empty($item_id)) {
            $item['PAYMENTREQUEST_0_INVNUM'] = $item_id;
        }

        // Now we will call SetExpressCheckout API operation. 
        // All available parameters for SetExpressCheckout are available at https://developer.paypal.com/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP/
        $response = $paypal->SetExpressCheckout($requestParams + $orderParams + $item);

        // Response is also accessible by calling  $paypal->getLastServerResponse()

        if (is_array($response) && $response['ACK'] == 'Success') { //Request successful
            //Now we have to redirect user to the PayPal
            //This is the point where user will be redirected to the PayPal page in order to provide Login details
            //After providing Login details, and after he confirms order in PayPal, user will be redirected to the page which you specified in RETURNURL field
            $token = $response['TOKEN'];

            $this->redirect()->toUrl($this->config['GlidePayments']['Paypal']['expressCheckoutURL'] . urlencode($token));
        } else if (is_array($response) && $response['ACK'] == 'Failure') {
            var_dump($response);
            exit;
        } else {
            var_dump($response);
            exit;
        }
    }

    /**
     * This route is called by Paypal after a payment attempt has been made at paypal.
     * User Id and Return will be present in the route parameters. Other information about
     * the transaction is retreived using the API
     *
     * @return null
     */
    public function captureAction() 
    {
        $return_url = $this->params()->fromQuery('return', null);

        $user_id = $this->params()->fromRoute('user', null);

        if (empty($user_id) || empty($return_url)) {
            throw new \Exception('Parameters missing');
        }

        $return_url = rawurldecode($return_url);
        
        // Retreive transaction info
        $token = $_GET["token"]; //Returned by paypal
        $paypal = new DPayPal($this->config['GlidePayments']['Paypal']);
        $requestParams = array('TOKEN' => $token);
        $response = $paypal->GetExpressCheckoutDetails($requestParams);
        $payerId = $response["PAYERID"]; //Paypal payer id returned by paypal
        //Create request for DoExpressCheckoutPayment
        $requestParams = array(
            "TOKEN" => $token,
            "PAYERID" => $payerId,
            "PAYMENTREQUEST_0_AMT" => $response["PAYMENTREQUEST_0_AMT"], //Payment amount. This value should be sum of of item values, if there are more items in order
            "PAYMENTREQUEST_0_CURRENCYCODE" => $response["PAYMENTREQUEST_0_CURRENCYCODE"], //Payment currency
            "PAYMENTREQUEST_0_ITEMAMT" => $response["PAYMENTREQUEST_0_ITEMAMT"]//Item amount
        );
        $transactionResponse = $paypal->DoExpressCheckoutPayment($requestParams); //Execute transaction


        if (is_array($transactionResponse) && $transactionResponse["ACK"] == "Success") {//Payment was successfull
            try {
                //Successful Payment
                $payment = new Payment();
                $payment->setAmount($response["PAYMENTREQUEST_0_AMT"]);
                $payment->setStatus(Payment::PAYMENT_STATUS_CONFIRMED);  // Received
                $payment->setVia(Payment::PAYMENT_VIA_PAYPAL); // Paypal
                $userRepo = $this->objectManager->getRepository($this->config['GlidePayments']['UserEntityName']);
                $payment->setUserTo($userRepo->find($user_id));
                $payment->setUserFrom($userRepo->find($this->config['GlidePayments']['Paypal_user_id']));
                $this->objectManager->persist($payment);
                $this->objectManager->flush();

                $msg = 'Your Paypal payment was successfully received';
            } catch (\Exception $ex) {
                \error_log('Problem creating payment. Payment acknowledged. PAYERID:'.$payerId);
            }
        } else {
            $msg = 'Problem receiving Paypal payment';
        }

        $this->flashMessenger()->addMessage($msg);

        if (empty(parse_url($return_url, PHP_URL_QUERY))) {
            $return_url .= '?msg=';
        } else {
            $return_url .= '&msg=';
        }

        $return_url .= $msg;

        return $this->redirect()->toUrl($return_url);
    }

}
