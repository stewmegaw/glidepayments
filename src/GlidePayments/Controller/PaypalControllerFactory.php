<?php
/**
 * PaypalController.php
 *
 * @category ControllerFactory
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\Aggregate\AggregateHydrator;
use Zend\Stdlib\Hydrator\ClassMethods;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

/**
 * PaypalControllerFactory creates a PaypmentController object
 * using dependency injection
 */
class PaypalControllerFactory implements FactoryInterface
{
    
    public function createService(ServiceLocatorInterface $serviceLocator) 
    {
        
        $objectManager = $serviceLocator->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $config = $serviceLocator->getServiceLocator()->get('Config');
        $authUserService = $serviceLocator->getServiceLocator()->get($config['GlidePayments']['UserAuthenticationService']);
        
        return new PaypalController($objectManager, $config, $authUserService);
    }
    
}