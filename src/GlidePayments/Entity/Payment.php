<?php
/**
 * Payment.php
 *
 * @category Entity
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * @ORM\Entity
 * @ORM\Table(name="payment")
 * @ORM\HasLifecycleCallbacks()
 */
class Payment
{

    const PAYMENT_STATUS_CONFIRMED = 1;
    
    const PAYMENT_VIA_PAYPAL = 1;
    const PAYMENT_VIA_INTERNAL = 2;
   
    
    /**
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlidePayments\Entity\UserInterface")
     */
    protected $userFrom;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlidePayments\Entity\UserInterface")
     */
    protected $userTo;

    /**
     *
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    protected $amount;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    protected $generatedTimestamp;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    protected $initiatedTimestamp;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    protected $transferedTimestamp;

    /**
     *
     * @ORM\Column(type="smallint")
     */
    protected $via;

    /**
     *
     * @ORM\Column(type="smallint")
     */
    protected $status;
    
    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlidePayments\Entity\PaymentEscrowed")
     */
    protected $escrow;
    
    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $userToPaypalEmail;
    
    public function __construct()
    {
        $this->generatedTimestamp = new \DateTime(); 
        $this->initiatedTimestamp = new \DateTime(); 
        $this->transferedTimestamp = new \DateTime(); 
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
        return $this;
    }

    public function getUserFrom() 
    {
        return $this->userFrom;
    }

    public function setUserFrom($userFrom) 
    {
        $this->userFrom = $userFrom;
        return $this;
    }
    
    public function getUserTo() 
    {
        return $this->userTo;
    }

    public function setUserTo($userTo) 
    {
        $this->userTo = $userTo;
        return $this;
    }

    public function getAmount() 
    {
        return $this->amount;
    }

    public function setAmount($amount) 
    {
        $this->amount = $amount;
        return $this;
    }

    public function getGeneratedTimestamp() 
    {
        return $this->generatedTimestamp;
    }

    public function setGeneratedTimestamp($generatedTimestamp) 
    {
        $this->generatedTimestamp = $generatedTimestamp;
        return $this;
    }

    public function getInitiatedTimestamp() 
    {
        return $this->initiatedTimestamp;
    }

    public function setInitiatedTimestamp($initiatedTimestamp) 
    {
        $this->initiatedTimestamp = $initiatedTimestamp;
        return $this;
    }

    public function getTransferedTimestamp() 
    {
        return $this->transferedTimestamp;
    }

    public function setTransferedTimestamp($transferedTimestamp) 
    {
        $this->transferedTimestamp = $transferedTimestamp;
        return $this;
    }

    public function getTransferVia() 
    {
        return $this->transferVia;
    }

    public function setTransferVia($transferVia) 
    {
        $this->transferVia = $transferVia;
        return $this;
    }

    public function getVia() 
    {
        return $this->via;
    }

    public function setVia($via) 
    {
        $this->via = $via;
        return $this;
    }

    public function getStatus() 
    {
        return $this->status;
    }

    public function setStatus($status) 
    {
        $this->status = $status;
        return $this;
    }
    
    public function getEscrow() 
    {
        return $this->escrow;
    }
    
    public function setEscrow($escrow) 
    {
        $this->escrow = $escrow;
        return $this;
    }
    
    public function getUserToPaypalEmail() 
    {
        return $this->userToPaypalEmail;
    }
    
    public function setUserToPaypalEmail($userToPaypalEmail) 
    {
        $this->userToPaypalEmail = $userToPaypalEmail;
        return $this;
    }

}
