<?php
/**
 * PaymentEscrowed.php
 *
 * @category Entity
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="payment_escrowed")
 * @ORM\HasLifecycleCallbacks()
 */
class PaymentEscrowed
{

    const PAYMENT_ESCROWED_TYPE_ACTIVE = 1;
    const PAYMENT_ESCROWED_TYPE_CANCELED = 2;
    const PAYMENT_ESCROWED_TYPE_COMPLETE = 3;
    const PAYMENT_ESCROWED_TYPE_ARB = 4;
    
    const PAYMENT_ESCROWED_ARB_CUSTOMER = 1;
    const PAYMENT_ESCROWED_ARB_PRODUCER = 2;

    /**
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlidePayments\Entity\UserInterface")
     */
    protected $user;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="GlidePayments\Entity\PaymentItemInterface")
     */
    protected $paymentItem;

    /**
     *
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    protected $amount;

    /**
     *
     * @ORM\Column(type="datetime")
     */
    protected $generatedTimestamp;

    /**
     *
     * @ORM\Column(type="smallint")
     */
    protected $status;
    
    /**
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    protected $arbitrationStatus;

    /**
     *
     * @ORM\Column(type="decimal", precision=6, scale=2, options={"default"=0})
     */
    protected $producerFee;

    public function __construct() 
    {
        $this->generatedTimestamp = new \DateTime();
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
        return $this;
    }

    public function getUser() 
    {
        return $this->user;
    }

    public function setUser($user) 
    {
        $this->user = $user;
        return $this;
    }

    public function getPaymentItem() 
    {
        return $this->paymentItem;
    }

    public function setPaymentItem($paymentItem) 
    {
        $this->paymentItem = $paymentItem;
        return $this;
    }

    public function getGeneratedTimestamp() 
    {
        return $this->generatedTimestamp;
    }

    public function setGeneratedTimestamp($generatedTimestamp) 
    {
        $this->generatedTimestamp = $generatedTimestamp;
        return $this;
    }

    public function getStatus() 
    {
        return $this->status;
    }

    public function setStatus($status) 
    {
        $this->status = $status;
        return $this;
    }

    public function getProducerFee() 
    {
        return $this->producerFee;
    }

    public function setProducerFee($producerFee) 
    {
        $this->producerFee = $producerFee;
        return $this;
    }

    public function getAmount() 
    {
        return $this->amount;
    }

    public function setAmount($amount) 
    {
        $this->amount = $amount;
        return $this;
    }
    
    public function getArbitationStatus() 
    {
        return $this->arbitrationStatus;
    }

    public function setArbitrationStatus($arbitrationStatus) 
    {
        $this->arbitrationStatus = $arbitrationStatus;
        return $this;
    }

}
