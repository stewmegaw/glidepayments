<?php
/**
 * PaymentItemInterface.php
 *
 * @category Interface
 * @package  GlidePayments
 * @author   Stewart Megaw
 */
namespace GlidePayments\Entity;

interface PaymentItemInterface
{
    public function getAmount();
    public function getProducer();
}
